% TEMPLATE for Usenix papers, specifically to meet requirements of
%  USENIX '05
% originally a template for producing IEEE-format articles using LaTeX.
%   written by Matthew Ward, CS Department, Worcester Polytechnic Institute.
% adapted by David Beazley for his excellent SWIG paper in Proceedings,
%   Tcl 96
% turned into a smartass generic template by De Clarke, with thanks to
%   both the above pioneers
% use at your own risk.  Complaints to /dev/null.
% make it two column with no page numbering, default is 10 point

% Munged by Fred Douglis <douglis@research.att.com> 10/97 to separate
% the .sty file from the LaTeX source template, so that people can
% more easily include the .sty file into an existing document.  Also
% changed to more closely follow the style guidelines as represented
% by the Word sample file. 

% Note that since 2010, USENIX does not require endnotes. If you want
% foot of page notes, don't include the endnotes package in the 
% usepackage command, below.

% This version uses the latex2e styles, not the very ancient 2.09 stuff.
\documentclass[letterpaper,twocolumn,10pt]{article}
\usepackage{usenix, epsfig, endnotes, xspace, url}
\usepackage{tikz}
\usetikzlibrary{positioning, arrows, trees, calc}

\begin{document}

\newcommand{\grnet}{{\sc grnet}\xspace}


%don't want date printed
\date{}

%make title bold and 14 pt font (Latex default is non-bold, 16 pt)
\title{\Large \bf Pithos+: An Open Online Storage System}

%for single author (just remove % characters)
\author{
{\rm Antony Chazapis}\\
\and
{\rm Giorgios Verigakis}\\
\and
{\rm Sofia Papagiannaki}\\
\and
{\rm Giorgios Tsoukalas}\\
\and
{\rm Panos Louridas}\\
\and
{\rm Nektarios Koziris}\\
\and
\multicolumn{1}{p\textwidth}{\centering Greek Research and Technology Network (GRNET)}\\
% copy the following lines to add more authors
% \and
% {\rm Name}\\
%Name Institution
} % end author

% Use the following at camera-ready time to suppress page numbers.
% Comment it out when you first submit the paper for review.
\thispagestyle{empty}

\maketitle


\begin{abstract}
Pithos+ is an online storage service based on the OpenStack Object
Storage {\sc api} with several important extensions. It uses a
block-based mechanism to allow users to upload, download, and share
files, keep different versions of a file, and attach policies to them.
It follows a layered, modular implementation. Pithos+ was designed to
be used as a storage service by the total set of the Greek research
and academic community (counting tens of thousands of users) but is
free and open to use by anybody, under a BSD-2 clause license.
\end{abstract}

\section{Introduction}

In 2008 the Greek Research and Technology Network (\grnet) decided
to offer an online storage service to the Greek research and academic
community. The service, called Pithos, was implemented in 2008--2009,
and was made available in spring 2009. It now has more than
12,000 users.

In 2011 \grnet decided to offer a new, evolved online storage
service, to be called Pithos+. Pithos+ is designed to address the
main requirements expressed by the Pithos users in the first two years of
operation:
\begin{itemize}
\item Provide both a web-based client and native desktop clients for
  the most common operating systems.
\item Allow not only uploading, downloading, and sharing, but also
  synchronization capabilities so that uses are able to select folders
  and have then synchronized automatically with their online accounts.
\item Allow uploading of large files, regardless of browser
  capabilities (depending on the version,  browsers may place a 2
  GBytes upload limit).
\item Improve upload speed; not an issue as long as the user is on a
  computer connected to the \grnet backbone, but it becomes important
  over {\sc adsl} connections.
\item Allow access by
  non-Shibboleth~\endnote{\url{http://shibboleth.internet2.edu/}}.
  accounts. Pithos delegates user authentication to the Greek
  Shibboleth federation, in which all research and academic
  institutions belong. However, it is desirable to have the option to
  open up Pithos to non-Shibboleth authenticated users as well.
\item Use open standards as far as possible.
\end{itemize}   

In what follows we describe the main features of Pithos+, the elements
of its design and the capabilities it affords. We touch on related
work and we provide some discussion on our experiences and thoughts on
the future.

\section{Pithos+ Features}

Pithos+ is based on the OpenStack Object Storage {\sc api} (Pithos
used a home-grown {\sc api}). We decided to adopt an open standard
{\sc api} in order to leverage existing clients that implement the
{\sc api}. In this way, a user can access Pithos+ with a standard
OpenStack client---although users will want to use a Pithos+ client to
use features going beyond those offered by the OpenStack {\sc api}.
The strategy paid off during Pithos+ development itself, as we were
able to access and test the service with existing clients, while also
developing new clients based on open source OpenStack clients.

The major extensions on the OpenStack {\sc api} are:
\begin{itemize}
\item The use of block-based storage in lieu of an object-based one.
  OpenStack stores objects, which may be files, but this is not
  necessary---large files (longer than 5GBytes), for instance, must be
  stored as a series of distinct objects accompanied by a manifest.
  Pithos+ stores blocks, so objects can be of unlimited size.
\item Permissions on individual files and folders. Note that folders
  do not exist in the OpenStack {\sc api}, but are simulated by
  appropriate conventions, an approach we have kept in Pithos+ to
  avoid incompatibility.
\item Fully-versioned objects.
\item Metadata-based queries. Users are free to set metadata on their
  objects, and they can list objects meeting metadata criteria.
\item Policies, such as whether to enable object versioning and to
  enforce quotas. This is particularly important for sharing object
  containers, since the user may want to avoid running out of space
  because of collaborators writing in the shared storage.
\item Partial upload and download based on \texttt{HTTP} request
  headers and parameters.
\item Object updates, where data may even come from other objects
  already stored in Pithos+. This allows users to compose objects from
  other objects without uploading data.
\item All objects are assigned {\sc uuid}s on creation, which can be
  used to reference them regardless of their path location.
\end{itemize}

\section{Pithos+ Design}

Pithos+ is built on a layered architecture (see Figure~\ref{fig:1}).
The Pithos+ server speaks {\sc http} with the outside world. The {\sc
  http} operations implement an extended OpenStack Object Storage {\sc
  api}. The back end is a library meant to be used by internal code and
other front ends. For instance, the back end library, apart from being
used in Pithos+ for implementing the OpenStack Object Storage {\sc
  api}, is also used in our implementation of the OpenStack Image
Service {\sc api}. Moreover, the back end library allows specification
of different namespaces for metadata, so that the same object can be
viewed by different front end {\sc api}s with different sets of
metadata. Hence the same object can be viewed as a file in Pithos+,
with one set of metadata, or as an image with a different set of
metadata, in our implementation of the OpenStack Image Service.

The data component provides storage of block and the information
needed to retrieve them, while the metadata component is a database of
nodes and permissions. At the current implementation, data is saved to
the filesystem and metadata in an {\sc sql} database. In the future,
data will be saved to some distributed block storage (we are currently
evaluating {\sc rados}~\cite{Weil:2007}), and metadata to a {\sc
  n}o{\sc sql} database.

\begin{figure}[t]
\begin{tikzpicture}
  [on grid, auto, 
  box/.style={rectangle,fill=white,draw=black,thick, align=center,
  inner ysep=0.5em}]
  \node at (0, 0) [box, text width=0.8\columnwidth] (api) {{\sc api} Front End};
  \node at (0, -1) [box, text width=0.8\columnwidth] (backend)
  {Back End} edge [<->] (api);
  \node at (-1, -2) [box, text width=0.2\columnwidth] (data) {Data} 
  edge [<->] (backend);
  \node at (1, -2) [box, text width=0.2\columnwidth]
  (metadata) {Metadata} edge [<->] (backend);
\end{tikzpicture}
\caption{Pithos+ Layers}
\label{fig:1}
\end{figure}

\subsection{Block-based Storage for the Client}

Since an object is saved as a set of blocks in Pithos+, object
operations are no longer required to refer to the whole object. We can
handle parts of objects as needed when uploading, downloading, or
copying and moving data.

In particular, a client, provided it has access permissions, can
download data from Pithos+ by issuing a \texttt{GET} request on an
object. If the request includes the \texttt{hashmap} parameter, then the
request refers to a \emph{hashmap}, that is, a set containing the
object's block hashes. The reply is of the form:
\begin{verbatim}
{"block_hash": "sha1", 
"hashes":
["7295c41da03d7f916440b98e32c4a2a39351546c", 
...], 
"block_size":131072, "bytes": 242}
\end{verbatim}
The client can then compare the hashmap with the hashmap computed from
the local file. Any missing parts can be downloaded with \verb+GET+
requests with an additional \verb+Range+ header containing the hashes
of the blocks to be retrieved. The integrity of the file can be
checked against the \verb+X-Object-Hash+ header, returned by the
server and containing the root Merkle hash of the object's
hashmap~\cite{Merkle:1987}.

When uploading a file to Pithos+, only the missing blocks will be
submitted to the server, with the following algorithm:
\begin{itemize}
\item Calculate the hash value for each block of the object to be
  uploaded.
\item Send a hashmap \verb+PUT+ request for the object. This is a
  \verb+PUT+ request with a \verb+hashmap+ request parameter appended
  to it. If the parameter is not present, the object's data (or part
  of it) is provided with the request. If the parameter is present,
  the object hashmap is provided with the request.
\item If the server responds with status 201 (Created), the blocks are
  already on the server and we do not need to do anything more.
\item If the server responds with status 409 (Conflict), the server’s
  response body contains the hashes of the blocks that do not exist on
  the server. Then, for each hash value in the server’s response (or all
  hashes together) send a \verb+POST+ request to the server with the
  block's data.
\end{itemize}

In effect, we are deduplicating data based on their block hashes,
transparently to the users. This results to perceived instantaneous
uploads when material is already present in Pithos+ storage.

\subsection{Block-based Storage Processing}

Hashmaps themselves are saved in blocks. All blocks are persisted to
storage using content-based addressing. It follows that to read a
file, Pithos+ performs the following operations:
\begin{itemize}
\item The client issues a request to get a file, via \texttt{HTTP
    GET}.
\item The {\sc api} front end asks from the back end the metadata
  of the object.
\item The back end checks the permissions of the object and, if they
  allow access to it, returns the object's metadata.
\item The front end evaluates any \texttt{HTTP} headers (such as
  \texttt{If-Modified-Since}, \texttt{If-Match}, etc.).
\item If the preconditions are met, the {\sc api} front end requests
  from the back end the object's hashmap (hashmaps are indexed by the
  full path).
\item The back end will read and return to the {\sc api} front end the
  object's hashmap from the underlying storage.
\item Depending on the \texttt{HTTP} \texttt{Range} header, the 
  {\sc api} front end asks from the back end the required blocks, giving
  their corresponding hashes.
\item The back end fetches the blocks from the underlying storage,
  passes them to the {\sc api} front end, which returns them to the client.
\end{itemize}

Saving data from the client to the server is done in several different
ways.

First, a regular \texttt{HTTP PUT} is the reverse of the \texttt{HTTP
  GET}. The client sends the full object to the {\sc api} front end.
The {\sc api} front end splits the object to blocks. It sends each
block to the back end, which calculates its hash and saves it to
storage. When the hashmap is complete, the {\sc api} front end commands
the back end to create a new object with the created hashmap and any
associated metadata.

Secondly, the client may send to the {\sc api} front end a hashmap and
any associated metadata, with a special formatted \texttt{HTTP PUT},
using an appropriate \texttt{URL} parameter. In this case, if the
back end can find the requested blocks, the object will be created as
previously, otherwise it will report back the list of missing blocks,
which will be passed back to the client. The client then may send the
missing blocks by issuing an \texttt{HTTP POST} and then retry the
\texttt{HTTP PUT} for the hashmap. This allows for very fast uploads,
since it may happen that no real data uploading takes place, if the
blocks are already in data storage.

Copying objects does not involve data copying, but is performed by
associating the object's hashmap with the new path. Moving objects, as
in OpenStack, is a copy followed by a delete, again with no real data
being moved.

Updates to an existing object, which are not offered by OpenStack, are
implemented by issuing an \texttt{HTTP POST} request including the
offset and the length of the data. The {\sc api} front end requests
from the back end the hashmap of the existing object. Depending on the
offset of the update (whether it falls within block boundaries or not)
the front end will ask the back end to update or create new blocks. At
the end, the front end will save the updated hashmap. It is also
possible to pass a parameter to \texttt{HTTP POST} to specify that the
data will come from another object, instead of being uploaded by the
client. 

\subsection{Pithos+ Back End Nodes}

Pithos+ organizes entities in a tree hierarchy, with one tree node per
path entry (see Figure~\ref{fig:2}). Nodes can be \emph{accounts},
\emph{containers}, and \emph{objects}. A user may have multiple
accounts, each account may have multiple containers, and each
container may have multiple objects. An object may have multiple
versions, and each version of an object has properties (a set of fixed
metadata, like size and mtime) and arbitrary metadata.

\begin{figure}[t]
\begin{tikzpicture}[%
grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
edge from parent path={(\tikzparentnode.south) |-(\tikzchildnode.west)},
version/.style={circle}]
\tikzstyle{every node}=[draw=black,thick,anchor=west]
\node {account}
  child { node {container}
    child { node(o1) {object} {
        child[grow=right]  { node(v1) [version] {$v1$} edge from
          parent[draw=none]
          child[grow=right] { node(v2) [version] {$v2$} edge from 
parent[draw=none]
          }}}}
    child { node {object}}
  }
child [missing] {}
child [missing] {}
child { node {container}};
\begin{scope}[dashed]
  \draw (o1)--(v1);
  \draw (v1)--(v2);
\end{scope}
\draw ($(v1)!0.5!(v2)$) circle [x radius=2cm, y radius=1cm];
\node[draw=none, anchor=center] at ($(v1)!0.5!(v2) + (0, -1.5cm)$) {versions};
\end{tikzpicture}
\caption{Pithos+ Back End Nodes}
\label{fig:2}
\end{figure}

The tree hierarchy has up to three levels, since, following the
OpenStack {\sc api}, everything is stored as an object in a container.
The notion of folders or directories is through conventions that
simulate pseudo-hierarchical folders. In particular, object names that
contain the forward slash character and have an accompanying marker
object with a \texttt{Content-Type: application/directory} as part of
their metadata can be treated as directories by Pithos+ clients. Each
node corresponds to a unique path, and we keep its parent in the
account / container / object hierarchy (that is, all objects have a
container as their parent).

\subsection{Pithos+ Back End Versions}

For each object version we keep the root Merkle hash of the object it
refers to, the size of the object, the last modification time and the
user that modified the file, and its \emph{cluster}. A version belongs
to one of the following three clusters (see Figure~\ref{fig:3}):
\begin{itemize}
  \item normal, which are the current versions
  \item history, which contain the previous versions of an object
  \item deleted, which contain objects that have been deleted
\end{itemize}

\begin{figure}[t]
\begin{tikzpicture}
[node distance=0.5cm and 0.3cm,
inner sep=1mm,
object/.style={rectangle,thick,draw=black},
history/.style={circle,thick,draw=black},
normal/.style={circle,thick,draw=black},
deleted/.style={circle,thick,draw=black}]
\matrix (m) [column sep=0.4cm] 
{
  \node (n11) [object] {object}; & \node (n12) [history] {H}; & 
  \node (n13) [normal] {N}; &  & & &; \\
  \node (n21) [object] {object}; &  & \node (n22) [history] {H}; 
  & \node (n23) [deleted] {D}; &  &  &; \node (n24) [normal] {N}; \\
  \node (n31) [object] {object}; &  & \node (n32) [history] {H}; &  &
  &  \node (n33) [history] {H}; & \node (n34) [normal] {N}; \\
  \node (n41) [object] {object}; & \node (n42) [history] {H}; &  &  & 
  \node (n43) [deleted] {H}; & \node (n44) [history] {D}; &  \\
};
\draw [-,dashed] (n11) -- (n12);
\draw [-,dashed] (n12) -- (n13);
\draw [-,dashed] (n21) -- (n22);
\draw [-,dashed] (n22) -- (n23);
\draw [-,dashed] (n23) -- (n24);
\draw [-,dashed] (n31) -- (n32);
\draw [-,dashed] (n32) -- (n33);
\draw [-,dashed] (n33) -- (n34);
\draw [-,dashed] (n41) -- (n42);
\draw [-,dashed] (n42) -- (n43);
\draw [-,dashed] (n43) -- (n44);

\draw [->] (m.south west) -- (m.south east);
\node [below left=0.1 cm of m.south east] {time};
\end{tikzpicture}
\caption{Pithos+ Versions in Time}
\label{fig:3}
\end{figure}

This versioning allows Pithos+ to offer to its user time-based
contents listing of their accounts. In effect, this also allows them
to take their containers back in time. This is implemented
conceptually by taking a vertical line in Figure~\ref{fig:3} and
presenting to the user the state on the left side of the line.

\subsection{Pithos+ Back End Permissions}

Pithos+ recognizes read and write permissions, which can be granted to
individual users or groups of users. Groups as collections of users
created at the account level by users themselves, and are flat---a
group cannot contain or reference another group. Ownership of a file
cannot be delegated.

Pithos+ also recognizes a ``public'' permission, which means that the
object is readable by all. When an object is made public, it is
assigned a {\sc url} that can be used to access the object from
outside Pithos+ even by non-Pithos+ users. 

Permissions can be assigned to objects, which may be actual files, or
directories. When listing objects, the back end uses the permissions as
filters for what to display, so that users will see only objects to
which they have access. Depending on the type of the object, the
filter may be exact (plain object), or a prefix (like \url{path/*} for
a directory). When accessing objects, the same rules are used to
decide whether to allow the user to read or modify the object or
directory. If no permissions apply to a specific object, the back end
searches for permissions on the closest directory sharing a common
prefix with the object.

\section{Related Work}

Commercial cloud providers have been offering online storage for quite
some time, but the code is not published and we do not know the
details of their implementation. Rackspace has used the OpenStack
Object Storage in its Cloud Files product. Swift is an open source
implementation of the OpenStack Object Storage {\sc api}. As we have
pointed out, our implementation maintains compatibility with
OpenStack, while offering additional capabilities.

\section{Discussion}

Pithos+ is implemented in Python as a Django application. We use {\sc
  sql}alchemy as a database abstraction layer. It is currently about
17,000 lines of code, and it has taken about 50 person months of
development effort. This development was done from scratch, with no
reuse of the existing Pithos code. That service was written in the
{\sc j2ee} framework. We decided to move from {\sc j2ee} to Python for
two reasons: first, {\sc j2ee} proved an overkill for the original
Pithos service in its years of operation. Secondly, Python was
strongly favored by the \grnet operations team, who are the people
taking responsibility for running the service---so their voice is
heard.

Apart from the service implementation, which we have been describing
here, we have parallel development lines for native client tools on
different operating systems (MS-Windows, Mac OS X, Android, and iOS).
The desktop clients allow synchronization with local directories, a
feature that existing users of Pithos have been asking for, probably
influenced by services like DropBox. These clients are offered in
parallel to the standard Pithos+ interface, which is a web application
build on top of the {\sc api} front end---we treat our own web
application as just another client that has to go through the {\sc
  api} front end, without granting it access to the back end directly.

We are carrying the idea of our own services being clients to Pithos+
a step further, with new projects we have in our pipeline, in which a
digital repository service will be built on top of Pithos+. It will
use again the {\sc api} front end, so that repository users will have
all Pithos+ capabilities, and on top of them we will build additional
functionality such as full text search, Dublin Core metadata storage
and querying, streaming, and so on.

At the time of this writing (March 2012) Pithos+ is in alpha,
available to users by invitation. We will extend our user base as we
move to beta in the coming months, and to our full set of users in the
second half of 2012. We are eager to see how our ideas fare as we will
scaling up, and we welcome any comments and suggestions.

\section{Acknowledgments}

Pithos+ is financially supported by Grant 296114, ``Advanced Computing
Services for the Research and Academic Community'', of the Greek
National Strategic Reference Framework.
\section{Availability}

The Pithos+ code is available under a BSD 2-clause license from:
\begin{center}
{\tt https://code.grnet.gr/projects/pithos/repository}
\end{center}

\noindent
The code can also be accessed from its source repository:
\begin{center}
{\tt https://code.grnet.gr/git/pithos/}
\end{center}

\noindent
More information and documentation is available at:
\begin{center}
{\tt http://docs.dev.grnet.gr/pithos/latest/index.html}
\end{center}


{\footnotesize \bibliographystyle{acm}
\bibliography{ppshort}}

%\theendnotes

\end{document}
